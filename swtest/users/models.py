from django.db import models


class Group(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=200)
    description = models.TextField(default="")

    def __str__(self):
        return self.name

    @property
    def get_users_quantity(self):
        return  len(User.objects.filter(group=self))

class User(models.Model):
    id = models.AutoField(primary_key=True)
    group = models.ForeignKey(Group, on_delete=models.CASCADE, blank=True, null=True)
    username = models.CharField(max_length=100)
    created_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.username

    @property
    def group_name(self):
        return str(self.group)