import React from 'react';
import axios from 'axios';
import { Table, Col,  Button, Modal, Input, Form } from 'antd';

const Context = React.createContext()

    const columns = [{
              title: 'ID',
              dataIndex: 'id',
              key: 'id',
            },{
              title: 'Name',
              dataIndex: 'name',
              key: 'name',
            }, {
              title: 'Description',
              dataIndex: 'description',
              key: 'description',
            }, {
              title: 'Action',
              key: 'action',
              render: (text, record) => (
                <div>
                    <GroupForm
                        btitle="Edit"
                        mtitle="Edit group:"
                        gid={record.id}
                        gname={record.name}
                        groupDescription={record.description}
                        requestType="patch"
                    />
                        &nbsp;&nbsp;
                    <GroupForm
                        btitle="Delete"
                        btype="danger"
                        mtitle="Delete this group?"
                        gid={record.id}
                        gname={record.name}
                        groupDescription={record.description}
                        requestType="delete"
                        />
                </div>
              ),
            }];


class GroupList extends React.Component {

    state = {
        groups: []
    }

    componentDidMount() {
        this.updateData();
    }

    updateData = ()=>{

        axios.get('http://127.0.0.1:8000/api/groups/')
        .then(res => {
            this.setState({
                groups: res.data
            });
        })
       console.log("Im updateData!!!");

    }

    render() {

        return(

          <Context.Provider value={{updateData: this.updateData}}>
            <div>
                <Col style={{ textAlign: 'right' }}>
                    <GroupForm
                        btitle="Add group"
                        mtitle="Add new group:"
                        gname="new_group"
                        requestType="post"
                    />
                </Col>
                <Table columns={columns} dataSource={this.state.groups} />
            </div>
          </Context.Provider>

        )
    }
}



class GroupForm extends React.Component {
  static contextType = Context

  state = {
    visible: false,
    confirmLoading: false,
    groupName: this.props.gname,
    groupId: this.props.gid,
    groupDescription: this.props.groupDescription,
  }

  showModal = () => {

        if (this.props.requestType !== 'delete') {
              this.setState({ visible: true });
        } else {
                axios.get(`http://127.0.0.1:8000/api/groups/${this.props.gid}`)
                .then(res => {
                        console.log(res.data)
                        if (res.data.get_users_quantity === 0) {
                              this.setState({ visible: true });
                        } else {
                            alert( 'Some users are assigned to this group. Deletion is impossible' );
                        }
                })
        }
  }

  handleChangeGroupName = (event) => {
    const groupName = event.target.value;
    this.setState({ groupName });
  }

  handleChangeGroupDescription = (event) => {
    const groupDescription = event.target.value;
    this.setState({ groupDescription });
  }

  handleOk = (event) => {

    switch ( this.props.requestType ) {
        case 'post':
                axios.post(`http://127.0.0.1:8000/api/groups/`,
                      {
                        name: this.state.groupName,
                        description: this.state.groupDescription

                      })
                    .then(res => {console.log(res);
                                    this.context.updateData();
                                 })
                    .catch(error => console.log(error));
                this.context.updateData();
           break;
        case 'patch':
                axios.patch(`http://127.0.0.1:8000/api/groups/${this.props.gid}/`,
                      {
                        name: this.state.groupName,
                        description: this.state.groupDescription

                      })
                    .then(res => {console.log(res);
                                      this.context.updateData();
                                 })
                    .catch(error => console.log(error));
                this.context.updateData();
           break;
        case 'delete':
                axios.delete(`http://127.0.0.1:8000/api/groups/${this.props.gid}`)
                     .then(res => {console.log(res);
                                       this.context.updateData();
                                  })
                     .catch(error => console.log(error));
           break;
        default:
            break
    }

    this.setState({
      confirmLoading: true,
    });
    setTimeout(() => {
      this.setState({
        visible: false,
        confirmLoading: false,
      });
    }, 50);

  }


  handleCancel = () => {
    console.log('Clicked cancel button');
    this.setState({
      visible: false,
    });
  }


  render() {
    const { visible, confirmLoading } = this.state;
    return (
      <span>
        <Button type={this.props.btype} onClick={this.showModal}>
          {this.props.btitle}
        </Button>

        <Modal
          title={this.props.mtitle}
          visible={visible}
          onOk={(event) => this.handleOk(event)}
          confirmLoading={confirmLoading}
          onCancel={this.handleCancel}
        >

      <Form onSubmit={this.handleSubmit}>
            <Form.Item label="Name" required="true" labelCol={{ span: 5 }} wrapperCol={{ span: 12 }}>
                  <Input
                  name="newgroupname"
                  defaultValue={this.state.groupName}
                  onChange={(event) => this.handleChangeGroupName(event)}
                  placeholder="Enter name"
                  />
            </Form.Item>

            <Form.Item label="Description" labelCol={{ span: 5 }} wrapperCol={{ span: 12 }}>
                  <Input
                  name="newgroupdescription"
                  defaultValue={this.state.groupDescription}
                  onChange={(event) => this.handleChangeGroupDescription(event)}
                  placeholder="Enter description"
                  />
            </Form.Item>
      </Form>

        </Modal>
      </span>
    );
  }
}


export default GroupList;




