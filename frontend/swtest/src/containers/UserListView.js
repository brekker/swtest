import React from 'react';
import axios from 'axios';
import { Table, Col,  Button, Modal, Select, Input, Form } from 'antd';

const { Option } = Select;
const Context = React.createContext()


    const columns = [{
              title: 'Username',
              dataIndex: 'username',
              key: 'username',
            }, {
              title: 'Created',
              dataIndex: 'created_date',
              key: 'created_date',
            }, {
              title: 'Group',
              dataIndex: 'group_name',
              key: 'group_name',
            }, {
              title: 'Action',
              key: 'action',
              render: (text, record) => (
                <div>
                    <UserForm
                        btitle="Edit"
                        mtitle="Edit user:"
                        uname={record.username}
                        uid={record.id}
                        gname={record.group_name}
                        gid={record.group}
                        requestType="patch"
                    />
                        &nbsp;&nbsp;
                    <UserForm
                        btitle="Detele"
                        btype="danger"
                        mtitle="Delete this user?"
                        uname={record.username}
                        uid={record.id}
                        gname={record.group_name}
                        requestType="delete"
                        />
                </div>
              ),
            }];

class UserList extends React.Component {

    state = {
        users: []
    }

    componentDidMount() {
        this.updateData();
    }

    updateData = ()=>{

        axios.get('http://127.0.0.1:8000/api/users/')
        .then(res => {
            this.setState({
                users: res.data
            });
        })
       console.log("Im updateData!!!");

    }

    render() {

        return(

          <Context.Provider value={{updateData: this.updateData}}>
            <div>
                <Col style={{ textAlign: 'right' }}>
                    <UserForm
                        btitle="Add user"
                        mtitle="Add new user:"
                        uname="new_username"
                        requestType="post"
                    />
                </Col>
                <Table columns={columns} dataSource={this.state.users} />
            </div>
          </Context.Provider>

        )
    }
}







class UserForm extends React.Component {
  static contextType = Context

  state = {
    visible: false,
    confirmLoading: false,
    usernameValue: this.props.uname,
    nameValue: this.props.uname,
    groupValue: this.props.gid,
    groups: [],
  }


  showModal = () => {
    this.setState({
      visible: true,
    });

        axios.get('http://127.0.0.1:8000/api/groups/')
            .then(res => {
                this.setState({
                    groups: res.data
                });
            })

  }

  handleChangeUsername = (event) => {
    const nameValue = event.target.value;
    this.setState({ nameValue });
  }


  handleChangeGroup = (groupValue) => {
    this.setState({ groupValue });
  }


  handleOk = (event) => {

    switch ( this.props.requestType ) {
        case 'post':
                axios.post(`http://127.0.0.1:8000/api/users/`,
                      {
                        group: this.state.groupValue,
                        username: this.state.nameValue

                      })
                    .then(res => {console.log(res);
                                    this.context.updateData();
                                 })
                    .catch(error => console.log(error));
                this.context.updateData();
           break;
        case 'patch':
                axios.patch(`http://127.0.0.1:8000/api/users/${this.props.uid}/`,
                      {
                        group: this.state.groupValue,
                        username: this.state.nameValue

                      })
                    .then(res => {console.log(res);
                                      this.context.updateData();
                                 })
                    .catch(error => console.log(error));
                this.context.updateData();
           break;
        case 'delete':
                axios.delete(`http://127.0.0.1:8000/api/users/${this.props.uid}`)
                     .then(res => {console.log(res);
                                       this.context.updateData();
                                  })
                     .catch(error => console.log(error));
           break;
        default:
            break
    }

    console.log(this.state.nameValue);
    console.log(this.state.groupValue);



    this.setState({
      confirmLoading: true,
    });
    setTimeout(() => {
      this.setState({
        visible: false,
        confirmLoading: false,
      });
    }, 50);

  }


  handleCancel = () => {
    console.log('Clicked cancel button');
    this.setState({
      visible: false,
    });
  }


  render() {
    const { visible, confirmLoading, groups } = this.state;
    return (
      <span>
        <Button type={this.props.btype} onClick={this.showModal}>
          {this.props.btitle}
        </Button>

        <Modal
          title={this.props.mtitle}
          visible={visible}
          onOk={(event) => this.handleOk(event)}
          confirmLoading={confirmLoading}
          onCancel={this.handleCancel}
        >


      <Form onSubmit={this.handleSubmit}>

            <Form.Item label="Username" required="true" labelCol={{ span: 5 }} wrapperCol={{ span: 12 }}>
                  <Input name="newusername" defaultValue={this.state.usernameValue}  onChange={(event) => this.handleChangeUsername(event)}/>
            </Form.Item>

            <Form.Item label="Group" labelCol={{ span: 5 }} wrapperCol={{ span: 12 }}>
                  <Select
                    placeholder="Select group"
                    defaultValue={this.props.gname}
                    onChange={this.handleChangeGroup}
                  >
                    {groups.map(d => <Option key={d.id}>{d.name}</Option>)}
                  </Select>
            </Form.Item>
      </Form>

        </Modal>
      </span>
    );
  }
}

export default UserList;