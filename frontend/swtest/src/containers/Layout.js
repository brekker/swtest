import React from "react"
import { Layout, Menu} from 'antd';
import { Link } from 'react-router-dom';

const { Header, Content } = Layout;

const CustomLayout = (props) => {
    return (
       <Layout className="layout">
        <Header>
          <Menu
            theme="dark"
            mode="horizontal"
            defaultSelectedKeys={['1']}
            style={{ lineHeight: '64px' }}
          >
            <Menu.Item key="1"><Link to="/users/">Users</Link></Menu.Item>
            <Menu.Item key="2"><Link to="/groups/">Groups</Link></Menu.Item>
          </Menu>
        </Header>
        <Content style={{ padding: '0 50px' }}>
              <div style={{ background: '#fff', padding: 24, minHeight: 280 }}>
                  {props.children}
              </div>
        </Content>
      </Layout>
    );
}

export default CustomLayout;