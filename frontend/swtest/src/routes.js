import React from 'react';
import { Route } from 'react-router-dom';

import UserList from './containers/UserListView';
import GroupList from './containers/GroupListView';


const BaseRouter = () => (
    <div>
        <Route exact path='/users/' component={UserList} />
        <Route exact path='/groups/' component={GroupList} />
    </div>
);

export default BaseRouter;