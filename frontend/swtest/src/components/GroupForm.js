import {
  Form, Select, Input, Button,
} from 'antd';
import React from 'react';


const { Option } = Select;

class GroupForm extends React.Component {

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
      }
    });
  }

  render() {
    return (
     <div>
      <Form onSubmit={this.handleSubmit}>

        <Form.Item label="Name" labelCol={{ span: 5 }} wrapperCol={{ span: 12 }}>
              <Input placeholder = "Enter group name" defaultValue={this.props.username}/>
        </Form.Item>
        <Form.Item label="Gender" labelCol={{ span: 5 }} wrapperCol={{ span: 12 }}>
              <Input placeholder = "Enter group description" defaultValue={this.props.username}/>
        </Form.Item>
        <Form.Item wrapperCol={{ span: 12, offset: 5 }}>
              <Button type="primary" htmlType="submit">
                Submit
              </Button>
        </Form.Item>
      </Form>
    </div>
    );
  }
}

export default UserForm;
